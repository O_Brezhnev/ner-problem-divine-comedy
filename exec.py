import pandas as pd
import string
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
import stanza
from sklearn.metrics import f1_score
from tqdm import tqdm

#stanza.download('ru')
stanza_nlp = stanza.Pipeline(lang='ru', processors='tokenize,ner')



df = pd.read_fwf('source.txt', header=None)
df.columns = ['text']

# stops = set(stopwords.words('russian'))
# def preprocess(textstring, stop_words):
#     punctuations = list(string.punctuation)
#     tokens = word_tokenize(textstring)
#     return [token.lower() for token in tokens if token.lower().isalpha()
#             and token.lower() not in stop_words and token.lower() not in punctuations]
#
# df_clear_series = df['text'].apply(lambda x: preprocess(x, stops))
# df_clear = pd.DataFrame(df_clear_series)
#
#
# print(df_clear)

f_score_dict ={'STANZA':[]}

def preproc(items):
    print(items)
    results = []
    for item in items:
        results.append(name_recognize_stanza(item))
    return results

def name_recognize_stanza(text):
    result = ''
    global stanza_nlp
    doc = stanza_nlp(text)
    for sent in doc.sentences:
        for ent in sent.ents:
            if ent.type == 'PER':
                result +=  f'entity: {ent.text}\ttype: {ent.type}' + " "
    if result != '':
        return result
    else: return 0

for i in tqdm(range(0,20)):
    # df_clear['STANZA'] = df_clear.apply(lambda x: preproc(x.values))
    # df_clear['STANZA_mark'] = df_clear.STANZA.apply(lambda x: x if x == 0 else 1)
    df['STANZA'] = df.apply(lambda x: (x.values))

#    f_score_dict['STANZA'].append(f1_score(df.IS_HUMAN, df.STANZA_mark))

print(df)
import pandas as pd
import string
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
import stanza
from sklearn.metrics import f1_score
import pymorphy2

#stanza.download('ru')
nlp = stanza.Pipeline(lang='ru', processors='tokenize,ner')
stops = set(stopwords.words('russian'))
morph = pymorphy2.MorphAnalyzer()


def preprocess(textstring, stop_words):
    punctuations = list(string.punctuation)
    tokens = word_tokenize(textstring)
    return [token for token in tokens if token.isalpha() and token.lower() not in stop_words and token not in punctuations]

df = pd.read_fwf('source.txt', header=None)
df.columns = ['text']

df_clear_series = df['text'].apply(lambda x: preprocess(x, stops))



ner_data = []
for i in df_clear_series:
    doc = nlp(' '.join(i))

    result = [f'{token.text}:{token.ner}' for sent in doc.sentences for token in sent.tokens if token.ner != 'O']
    if result != []:
        ner_data.append(result[0].split(sep=':'))
        print(result[0])

ner_data_df = pd.DataFrame(ner_data, columns = ['token','ner'])

def define_morphs(data):
    morph_results = []
    for i in data:
        print("item:", i)
        parse_result = morph.parse(i)
        if len(parse_result) > 1:
            for parse_item in parse_result:
                if parse_item.tag.POS == 'NOUN':
                    morph_results.append(i)
                    break
        else:
            if parse_result[0].tag.POS == 'NOUN':
                morph_results.append(i)

    return morph_results

print(define_morphs(ner_data_df['token']))